<?php
	//configuration
	require("../includes/config.php");
	
	//if user visiting via Get
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		render("buyform.php", ["title" => "Buy Page"]);
	}
	
	//if a symbol to buy was submitted
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		//if form field was empty
		if(empty($_POST["stock"]) || empty($_POST["numberofshares"]))
		{
			apologize("One or two fields were left empty", $_SERVER["PHP_SELF"]);
		}
		
		//if number of shares given was a number
		if(!is_int((int)$_POST["numberofshares"]))
		{
			apologize("Invalid number of shares provided", $_SERVER["PHP_SELF"]);
		}
		
		//storing the values in short named variable
		$shares = $_POST["numberofshares"];
		$symbol = $_POST["stock"];
		
		// validating the symbol submitted and continuing the process
		$stock = lookup($symbol);
		
		if($stock == false)
		{
			apologize("Invalid stock symbol", $_SERVER["PHP_SELF"]);
		}
		else
		{
			//total cost for what user wants to buy
			$price = $stock["price"] * $shares;
			
			//preparing sql to get what cash user has
			$sql = sprintf("SELECT * FROM users WHERE id = '%s'", $_SESSION["id"]);
			
			//executing
			$result = mysql_query($sql);
			
			$row = mysql_fetch_assoc($result);
			
			if($row["cash"] < $price)
			{
				apologize("You have insufficent money to buy these shares", $_SERVER["PHP_SELF"]);
			}
			else
			{
				// preparing sql for transaction
				$sql = sprintf("INSERT INTO portfolios (userid, symbol,
								shares) VALUES('%s', '%s', '%s') ON 
								DUPLICATE KEY UPDATE shares = shares + VALUES(shares)",
								$_SESSION["id"], $symbol, $shares);
				
				$sql2 = sprintf("UPDATE users SET cash = cash - %s", $price);
				
				//starting transaction
				mysql_query("START TRANSACTION");
				
				$a1 = mysql_query($sql);
				$a2 = mysql_query($sql2);
				
				if($a1 && $a2)
				{
					mysql_query("COMMIT");
					
					//store in history if transaction was a sucess
					$worth = $shares * $stock["price"];
					
					$sqlforhistory = sprintf("INSERT INTO history (userid, date, action,
					symbol, company, shares, price, worth) VALUES ('%s', NOW(),
					'BUY', '%s', '%s', '%s', '%s', '%s')",$_SESSION["id"], $stock["symbol"],
					$stock["name"], $shares, $stock["price"], $worth);
					
					
					//executing query
					mysql_query($sqlforhistory);
				}
				else
				{
					mysql_query("ROLLBACK");
				}
				
				
				//redirecting to home
				redirect();				
			}	
		}	
				
	}	
?>
