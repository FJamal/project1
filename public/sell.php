<?php
	
	//configuration
	require("../includes/config.php");
	
	// if user visited via get than display selling form
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		//prepaing sql to get all the symbols of stocks the user owns
		$sql = sprintf("SELECT symbol FROM portfolios WHERE userid = '%s'", $_SESSION["id"]);
		
		//executing the sql
		$result = mysql_query($sql);
		
		$rows= [] ;
		
		// if a single row is there
		if(mysql_num_rows($result) > 0)
		{
			//iterating until there is no row
			while($row = mysql_fetch_assoc($result))
			{
				// adding each result as an associative array
				$rows [] = [
					"symbol" => strtoupper($row["symbol"])
					];
			}
			
			//render("sellform.php", ["title" => "Sell Your Stocks", "stocktosell" => $rows]);
		}
		
		render("sellform.php", ["title" => "Sell Your Stocks", "stocktosell" => $rows]);
				
	}
	
	//if user submitted a symbol to sell
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		//preparing sql to get the number of shares the user have
		$sql = sprintf("SELECT shares FROM portfolios WHERE userid = '%s'
						AND symbol = '%s'", $_SESSION["id"], $_POST["symbol"]);
						
		
		//executing query
		$result = mysql_query($sql);
		
		// fetching as a associative array
		$row = mysql_fetch_assoc($result);
		
		$shares = $row["shares"];
		
		
		// getting the current price of the selected share
		$stock = lookup($_POST["symbol"]);
		
		//calculating the cash user gets  after selling the shares
		$cash = $shares * $stock["price"];
		
		//sql to update history
		$sqlhistory = sprintf("INSERT INTO history (userid, date, action,
					symbol, company, shares, price, worth) VALUES ('%s', NOW(),
					'SELL', '%s', '%s', '%s', '%s', '%s')",$_SESSION["id"], $stock["symbol"],
					$stock["name"], $shares, $stock["price"], $cash);
					
					
		
		//preparing two sql for transaction
		$sql = sprintf("DELETE FROM portfolios WHERE userid = '%s' AND
						symbol = '%s'", $_SESSION["id"], $_POST["symbol"]);
						
		$sql2 = sprintf("UPDATE users SET cash = cash + %s", $cash);
		
		// starting transaction
		mysql_query("START TRANSACTION");
		$a1 = mysql_query($sql);
		$a2 = mysql_query($sql2);
		if($a1 && $a2)
		{
			mysql_query("COMMIT");
			
			//then updating the history
			mysql_query($sqlhistory);
		}
		else
		{
			mysql_query("ROLLBACK");
		}
		
		
		//redirecting to sell page
		redirect("sell.php");	 								
	}	
?>
