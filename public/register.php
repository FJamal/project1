<?php
	//configuration
	require("../includes/config.php");
	
	//if user visiting via GET
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		//display the register form
		render("register_form.php", ["title" => "Register"]);
	}
	
	//if user sumitted the form
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		// check if any of the field was empty
		if(empty($_POST["username"]) || empty($_POST["password"]) || empty($_POST["confirmation"]))
		{
			apologize("username or password was left empty");
		}
		else if($_POST["password"] != $_POST["confirmation"])
		{
			apologize("Passwords did not match", $_SERVER["PHP_SELF"]);
			
		}
		
		//hashing the password
		$password = password_hash(mysql_real_escape_string($_POST["password"]), PASSWORD_DEFAULT);
		
				
		//preparing sql
		$sql = sprintf("INSERT IGNORE INTO users (username, hash, cash) VALUES ('%s', '%s', '10000.0000')",
						mysql_real_escape_string($_POST["username"]),
						$password);
		
		
		/*storing the entered user name so that it can be refrenced 
		 * back to get the id number once registeration is a success*/
		 $currentusername = mysql_real_escape_string($_POST["username"]);
		 
		
		//executing the query
		$result = mysql_query($sql);
		
		//checking how many rows were affected
		$numrow = mysql_affected_rows();
		
		if($numrow == 1)
		{
			$sqlforsession = sprintf("SELECT * FROM users WHERE username = '%s'",
									  $currentusername);
			
			//executing it
			$result = mysql_query($sqlforsession);
			
			//fetching the result as an array
			$row = mysql_fetch_array($result);						  
			
			//setting up seeions so that it can redirected and index.php could be loaded
			$_SESSION["id"] = $row["id"];
			
			//redirecting
			redirect();
		}
		else
		{
			apologize("username already in use", $_SERVER["PHP_SELF"]);
		}									
	}		
?>
