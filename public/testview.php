<!DOCTYPE html>
<html>
	<head>
		<title>CS50 Finance</title>
		<style>
		body
		{
			font-family : Georgia, Times, serif;
			color: #fff8dc;
			background-color: #000;
			width: 90%;	
			heigth: 90%;
		}
		h1
		{
			width: 50%;
			background-color:red;
			font-size: 350%;
			text-shadow: -3px 2px 1px grey;
			position: relative;
			margin: 10% auto 10% auto;
		}
		#logo
		{
			width: 70%;
			margin: 6% 15% 6% 15%;	
		}
		input, label
		{
			display: block;
			margin-top: 3%;
		}
		form
		{
			width: 40%;
			margin: 0% auto 0% auto;
			
		}
		label
		{
			font-size: 20px;
		}
		input
		{
			width: 80%;
			padding: 5px 10px;
			font-size: 18px;
			background-color: #a9a9a9;
			
		}
		::-webkit-input-placeholder
		{ 
			/* Chrome/Opera/Safari */
			color: #f8f8ff;
		}
		input[type = "text"]:focus, input[type = "password"]:focus
		{
			background-color: #f8f8ff;
			color:#a9a9a9;
			font-size: 18px;
		}
		input:hover
		{
			background-color: #cccccc;
		}	
		fieldset
		{
			border: 2px double #e6ffcc;
			border-width: 4px;
			border-radius: 10px;
		}
		legend
		{
			font-size: 26px;
			background-color: #7cfc00;
			border: none;
			padding:5px;
			border-radius: 5px;
		}
		input[type = "submit"]
		{
			border: none;
			border-radius: 5px;
			background-color: #66cc00;
			width: 45%;
			color:#7cfc00;
			position: relative;
			top: 2px;
			left: 45%;
		}
		input[type = "submit"]:hover
		{
			background-color: #336600;
		}
		#reg
		{
			width: 40%;
			margin: 2% auto 0% auto;
			text-align: center;
		}
		a
		{
			text-transform: capitalize;
			text-decoration: none;
		}								
						
	</style>
	</head>
	
	<body>
		<img src = "img/CS75Finance.gif" alt = "logo" id = "logo">
		<form action = "login.php" method = "POST">
			<fieldset>
				<legend>Log In</legend>
				<label>Enter your username:
					<input type = "text" name = "username" placeholder = "username">
				</label>
				<label>Enter Your password:
					<input type = "password" placeholder = "password" name = "password">
				</label>
				<input type = "submit" value = "LogIn">
			</fieldset>
		</form>
		<div id = "reg">
			<p>or you can <a href ="register.php">register</a> for an account.</p>
		</div>
	</body>
</html>
