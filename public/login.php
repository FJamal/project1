<?php
	//configuration
	require("../includes/config.php");
	
	//if reached via GET
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		render("login_form.php", ["title" => "Login Page"]);
	}
	//else if it was post and username or password was submitted
	else if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		if(empty($_POST["username"]) || empty($_POST["password"]))
		{
			apologize("username or password was not submitted", $_SERVER["PHP_SELF"]);
		}
		else
		{
			//preparing sql
			$sql = sprintf ("SELECT * FROM users WHERE username = '%s'", mysql_real_escape_string($_POST["username"]));
		
			//execute the query
			$result = mysql_query($sql);
		
			// if username was there than 1 row would be returned
			if (mysql_num_rows($result) == 1)
			{
				//getting that row as an associative array
				$row = mysql_fetch_assoc($result);
				
				//checking if password matched
				if(password_verify($_POST["password"], $row["hash"]))
				{
					$_SESSION["id"] = $row["id"];
			
					//redirecting to index.php
					redirect();
				}	
			}	
		}
			apologize("invalid username or password", $_SERVER["PHP_SELF"]);
	}		
?>
