<?php
	
	//configuration
	require("../includes/config.php");
	
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		$history = [];
		
		$sql = sprintf("SELECT * FROM history WHERE userid = '%s'", $_SESSION["id"]);
		
		//executing
		$result = mysql_query($sql);
		
		//if there was a row
		if(mysql_num_rows($result) > 0)
		{
			while($row = mysql_fetch_assoc($result))
			{
				//storing rows as an associative array
				$history [] = [
					"date" => $row["date"],
					"action" => $row["action"],
					"symbol" => $row["symbol"],
					"company" => $row["company"],
					"shares" => $row["shares"],
					"price" => number_format($row["price"], 2 ,'.', ''),
					"worth" => number_format($row["worth"], 2, '.', ',')
					];
			}	
		}	
		
		//displayvariable($history);
		render("historyview.php", ["title" => "Your History", "data" => $history]);
	}	
?>
