<?php
	//configuration
	require("../includes/config.php");
	
	//visited via get
	if($_SERVER["REQUEST_METHOD"] == "GET")
	{
		render("quoteview.php", ["title" => "Get Quote"]);	
	}
	
	//if symbol was submitted
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		//calling the lookup function
		$stock = lookup($_POST["symbol"]);
		
		if($stock == false)
		{
			apologize("Invalid Stock Symbol", $_SERVER["PHP_SELF"]);
		}
		else
		{
			render("quoteprice.php", ["stock" => $stock, "title" => "The Price Is"]);
		}		
			
	}		
?>
