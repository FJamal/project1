<?php
	//addition of file config, configures it as to what to load and connect to db
	require("../includes/config.php");
	
	//preparing sql to get total cash in hand for the user
	$sql = sprintf("SELECT * FROM users WHERE id = '%s'", $_SESSION["id"]);
	
	//executing the query
	$result = mysql_query($sql);
	
	$row = mysql_fetch_assoc($result);
	
	$cashinhand = number_format($row["cash"], 2, '.', ',');
	
	//preparing sql to get shares of current user
	$sql = sprintf("SELECT * FROM portfolios WHERE userid = '%s'", $_SESSION["id"]);
	
	//executing query
	$result = mysql_query($sql);
	
	$rows = [];
	
	//if there is a rwo that is a share is there
	if(mysql_num_rows($result) > 0)
	{
		while($row = mysql_fetch_assoc($result))
		{
			// getting the name of current symbol via lookup function
			$name = lookup($row["symbol"]);
			
			//worth for each company
			$worth = number_format($name["price"] * $row["shares"], 2, '.', ',');
			
			// stroing every row as an array, which itself is an associative array
			$rows [] = [
				"name" => $name["name"],
				"price" => number_format($name["price"], 2, '.', ''),
				"symbol" => $name["symbol"],
				"shares" => $row["shares"],
				"worth" => $worth				
				];
						
		}	
		
		//displayvariable($rows);
	}	

	//loading the display page
	render("portfolio.php", ["title" => "Home", "data" => $rows, "cash" => $cashinhand]);
?>
