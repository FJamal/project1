<div id = "list">
	<ul id = "nav">
		<li><a href = "index.php">home</a></li>
		<li><a href = "quote.php">quote</a></li>
		<li><a href = "buy.php">buy</a></li>
		<li><a href = "sell.php">sell</a></li>
		<li><a href = "history.php">history</a></li>
		<li><a href = "logout.php">log out</a></li>
	</ul>
</div>
<form action = "buy.php" method = "POST">
	<fieldset>
		<legend>Buy Stock</legend>
		<label>
			Enter the symbol of stock:
			<input type = "text" name = "stock" placeholder ="symbol">
		</label>
		<label>
			Enter the number of shares:
			<input type = "text" name = "numberofshares" placeholder = "Enter a number"> 
		</label>
		<input type = "submit" value = "buy it">
	</fieldset>
</form>
