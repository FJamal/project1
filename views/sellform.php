<div id = "list">
	<ul id = "nav">
		<li><a href = "index.php">home</a></li>
		<li><a href = "quote.php">quote</a></li>
		<li><a href = "buy.php">buy</a></li>
		<li><a href = "sell.php">sell</a></li>
		<li><a href = "history.php">history</a></li>
		<li><a href = "logout.php">log out</a></li>
	</ul>
</div>

<?php if(empty($stocktosell)): ?>
<div class = "nostocks">
	<p>you have no stocks to sell</p>
</div>
<?php endif ?>

<?php if(!empty($stocktosell)): ?>
<form action = "sell.php" method = "POST">
	<fieldset>
		<legend>Stock to sell</legend>
		<select name = "symbol">
			<option selected = "true" disabled = "disabled">Symbol</option>
				<?php foreach($stocktosell as $item): ?>
					<option value = "<?= $item["symbol"] ?>"><?= $item["symbol"] ?></option>
				<?php endforeach ?>	
		</select>
		<input type = "submit" value = "sell it">
	</fieldset>
</form>
<?php endif ?>


