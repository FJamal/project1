<!DOCTYPE html>
<html>
	<head>
		<title><?= htmlspecialchars($title)?></title>
		<style>
		body
		{
			font-family : Georgia, Times, serif;
			color: #fff8dc;
			background-color: #000;
			width: 90%;	
			height: 90%;
			
		}
		h1
		{
			width: 50%;
			background-color:red;
			font-size: 350%;
			text-shadow: -3px 2px 1px grey;
			position: relative;
			margin: 10% auto 10% auto;
		}
		#logo
		{
			width: 70%;
			height: 20%;
			margin: 6% 15% 2% 15%;	
		}
		input, label
		{
			display: block;
			margin-top: 3%;
		}
		form
		{
			width: 40%;
			margin: 0% auto 0% auto;
			
		}
		label
		{
			font-size: 20px;
		}
		input, select
		{
			width: 80%;
			padding: 5px 10px;
			font-size: 18px;
			background-color: #a9a9a9;
			
		}
		::-webkit-input-placeholder
		{ 
			/* Chrome/Opera/Safari */
			color: #f8f8ff;
		}
		input[type = "text"]:focus, input[type = "password"]:focus
		{
			background-color: #f8f8ff;
			color:#a9a9a9;
			font-size: 18px;
		}
		input:hover, select:hover
		{
			background-color: #cccccc;
		}	
		fieldset
		{
			border: 2px double #e6ffcc;
			border-width: 4px;
			border-radius: 10px;
		}
		legend
		{
			font-size: 26px;
			background-color: #7cfc00;
			border: none;
			padding:5px;
			border-radius: 5px;
			text-transform: capitalize;
		}
		input[type = "submit"]
		{
			text-transform: capitalize;
			border: none;
			border-radius: 5px;
			background-color: #66cc00;
			width: 45%;
			color:#7cfc00;
			position: relative;
			top: 2px;
			left: 45%;
		}
		input[type = "submit"]:hover
		{
			background-color: #336600;
		}
		#apology
		{
			margin = 0% auto 0% auto;
			text-align: center;	
		}
		#reg
		{
			width: 40%;
			margin: 2% auto 0% auto;
			text-align: center;
		}
		a
		{
			text-transform: capitalize;
			text-decoration: none;
		}
		#list
		{
			width: 70%;
			margin: 0% 15% 6% 15%;
			text-align: center;
			
		}
		#list li
		{
			
			text-decoration: none;
			list-style-type: none;
			padding : 2%;
			font-size: 125%;
			display: inline;
			margin-right: 1%;
		}
		#list a
		{
			color: #fff8dc;
		}
		#list li:hover
		{
			background-color: #7cfc00;
			border-radius: 5%;
		}
		.currentpage
		{
			background-color: #66cc00;
			
		}
		table
		{
			width: 70%;
			margin: 6% 15% 2% 15%;
			border-collapse: collapse;
		}
		th
		{
			text-transform: uppercase;
			padding: 1%;
			background-color: #00008b;
			
		}
		tr
		{
			border: 1px solid #fff8dc; 
		}
		.odd td
		{
			text-align:center;
			background-color: #00ffff;
			padding: 1%;
			color: #000;
		}
		td
		{
			text-align:center;
			background-color:  #e6ffff;
			padding: 1%;
			color: #000;
		}
		#cash
		{
			text-align: left;
		}
		#total
		{
			text-align: right;
		}
		.nostocks
		{
			width: 70%;
			text-transform: capitalize;
			margin: 2% 15% 2% 15%;
			text-align: center;
			font-size: 120%;	
		}
		.odd td, td
		{
			font-size: 90%;
		}	
		#history
		{
			width: 96%;
			margin: 6% 2% 2% 2%;
		}
		
			
				
										
						
	</style>
	
	</head>
	
	<body>
		<img src = "img/CS75Finance.gif" alt = "logo" id = "logo">
