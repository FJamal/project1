<div id = "list">
	<ul id = "nav">
		<li><a href = "index.php">home</a></li>
		<li><a href = "quote.php">quote</a></li>
		<li><a href = "buy.php">buy</a></li>
		<li><a href = "sell.php">sell</a></li>
		<li><a href = "history.php">history</a></li>
		<li><a href = "logout.php">log out</a></li>
	</ul>
</div>
<?php if(empty($data)): ?>
<div class = "nostocks">
	<p>you currently have no history of transactions</p>
</div>
<?php endif ?>

<?php if(!empty($data)): ?>
<table id = "history">
	<thead>
		<tr>
			<th colspan = "7">Your history</th>
		</tr>
		<tr>
			<th>date</th>
			<th>action</th>
			<th>Company</th>
			<th>Symbol</th>
			<th>Shares</th>
			<th>price</th>
			<th>total</th>
		</tr> 
	</thead>
	<tbody>
		<?php $flag = 0 ?>
		<?php foreach($data as $item): ?>
			<?php switch($flag): ?>
<?php case 0: ?>
				<tr class = "odd">
					<td><?= $item["date"] ?></td>
					<td><?= $item["action"]?></td>
					<td><?= $item["company"]?></td>
					<td><?= $item["symbol"]?></td>
					<td><?= $item["shares"]?></td>
					<td>$ <?= $item["price"]?></td>
					<td>$ <?= $item["worth"]?></td>
				</tr>
				<?php $flag = 1;
					break;
				 ?>
			<?php default: ?>
					<tr>
						<td><?= $item["date"] ?></td>
						<td><?= $item["action"]?></td>
						<td><?= $item["company"]?></td>
						<td><?= $item["symbol"]?></td>
						<td><?= $item["shares"]?></td>
						<td>$ <?= $item["price"]?></td>
						<td>$ <?= $item["worth"]?></td>
					</tr>
			<?php $flag = 0;
				break;
			?>
			<?php endswitch ?>
		<?php endforeach ?>			
	</tbody>
</table>
<?php endif ?>
