<div id = "list">
	<ul id = "nav">
		<li><a href = "index.php">home</a></li>
		<li><a href = "quote.php">quote</a></li>
		<li><a href = "buy.php">buy</a></li>
		<li><a href = "sell.php">sell</a></li>
		<li><a href = "history.php">history</a></li>
		<li><a href = "logout.php">log out</a></li>
	</ul>
</div>
<?php if(empty($data)): ?>
<div class = "nostocks">
	<p>you have no stocks to display</p>
</div>
<?php endif ?>


<table>
	<thead>
		<tr>
			<th colspan = "5">your portfolio</th>
		</tr>
		<tr>
			<th>company</th>
			<th>symbol</th>
			<th>price</th>
			<th>shares</th>
			<th>total</th>
		</tr>	
	</thead>
	<tbody>
		<?php $flag = 0;?>
		<!--using php foreach to iterate over each data -->
		<?php foreach($data as $item):?>
			
			<?php switch($flag):?>
<?php case 0: ?>
				<tr class = "odd">
					<td><?= $item["name"] ?></td>
					<td><?= $item["symbol"] ?></td>
					<td>$<?= $item["price"] ?></td>
					<td><?= $item["shares"] ?></td>
					<td>$<?= $item["worth"] ?></td>
				</tr>
			<?php $flag = 1;
			break;?>
			<?php default: ?>
				<tr>
					<td><?= $item["name"] ?></td>
					<td><?= $item["symbol"] ?></td>
					<td>$<?= $item["price"] ?></td>
					<td><?= $item["shares"] ?></td>
					<td>$<?= $item["worth"] ?></td>
				</tr>
			<?php $flag = 0;
			break;?>
			<?php endswitch ?>
		<?php endforeach ?>
		<tr>
			<th colspan = "2" id = "cash">cash left</th>
			<th colspan = "3" id = "total">$<?= $cash ?></th>
		</tr>			
				
	</tbody>
</table>

