<?php
	/* All the Functions */
	
	// connecting to db with with mysql api
	if($connection = mysql_connect("localhost", "jharvard", "crimson") === false)
	{
		apologize("Could not connect to database");
	}	
	
	// select database
	if(mysql_select_db("cs50finance") === false)
	{
		apologize("Could not select database");
	}	
	
	
	/* function that redirects to certain page */
	function redirect($page)
	{
		/* Redirect to a different page in the current directory that was requested */
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = $page;
		header("Location: http://$host$uri/$extra");
		exit;
	}	
	
	/*function to load the viewpage and its variables */
	function render($view, $values = [])
	{
		if(file_exists("../views/{$view}"))
		{
			//extract the values passed to this page
			extract($values);
		    require("../views/header.php");
			require("../views/{$view}");
			require("../views/footer.php");
		}
		else
		{
			trigger_error("invalid view = {$view}", E_USER_ERROR);
		}		
	}	
	
	/*function to display an error message and location for go back link */
	function apologize($message, $link)
	{
		render("apology.php", ["message" => $message, "title" => "SORRY", "link" => $link]);
	}	
	
	/*function to display variables value*/
	function displayvariable($test)
	{
		render("printr.php", ["title" => "Display variable", "test" => $test]);
	}
	
	/*function to quote prices*/
	function lookup($symbol)
	{
		//url encoding the symbol
		$symbol = urlencode($symbol);
		
		// manually writting the url to visit
		$url = "http://download.finance.yahoo.com/d/quotes.csv?s={$symbol}&f=snl1&e=.csv";
		
		//opening the file
		$handle = fopen($url, "r");
		
		//storing the results
		$rows = fgetcsv($handle);
		
		//the 2nd and 3rd columns return a string N/A if unknown symbol
		if($rows[1] != "N/A" && $rows[2] != "N/A")
		{
			//that means the symbol is a legit one
			return ["symbol" => strtoupper($rows[0]),
					"name" => $rows[1],
					"price" => floatval($rows[2])];
			
			fclose($handle);		
		}
		else
		{
			fclose($handle);
			return false;
		}		
	}
	
	
	function logout()
    {
        // unset any session variables
        $_SESSION = [];

        // expire cookie
        if (!empty($_COOKIE[session_name()]))
        {
            setcookie(session_name(), "", time() - 42000);
        }

        // destroy session
        session_destroy();
    }		
?>
