<?php 
/*config file which would be included in all file that will attach the
 * function files to all the files and the sql functions
 * and limit the user from accessing anyother file through the url
 * except login.php */

	ini_set("display_errors", true);
	
	//turning off deprecated warning ^ E_DEPRECATED since using mysql api
	error_reporting(E_ALL ^ E_DEPRECATED);
	
	session_start();
	
	
	// including functions file
	require("helpers.php");
	
	

	
	/*to limit access to only these urls and not anyother by 
	  manually typing the urls*/
	if(!in_array($_SERVER["PHP_SELF"], ["/login.php", "/register.php"]))
	{
		if(empty($_SESSION["id"]))
		{
			redirect("login.php");
		}	
	}	   
?>	
